---
layout: handbook-page-toc
title: "Sales Operating Procedures"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Sales Operating Procedures

In this Sales Standard Operating Procedures (SOP) page, the four phases of the sales cycle are: Engage & Educate, Facilitate the Opportunity, Deal Closure, and Retain and Expand. This SOP is based on the GitLab Sales Process document approved in February 2020. Each phase will be covered with the step-by-step procedures necessary to accomplish each phase. 

1. [Engage and Educate the Customer](/handbook/sales/sales-operating-procedures/engage-and-educate-the-customer)
1. Facilitate the Opportunity (coming soon)
1. Deal Closure (coming soon)
1. Retain and Expand (coming soon)
