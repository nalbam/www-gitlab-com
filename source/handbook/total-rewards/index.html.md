---
layout: markdown_page
title: Total Rewards
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Introduction

Total Rewards at GitLab is made up of Benefits, Base Salary, Bonuses, Stock Options, and Perks.

## Benefits

[Benefits](/handbook/benefits/) at GitLab are determined based on our [Guiding Principles](/handbook/benefits/#guiding-principles). [General Benefits](/handbook/benefits/#general-benefits) (for example, [Paid time off](/handbook/paid-time-off/) policy) apply to all GitLab team members whereas [Entity Specific Benefits](/handbook/benefits/#entity-specific-benefits) apply based on the specific [contract type](/handbook/contracts/#employee-types-at-gitlab).

## Base Salary 

[Base Salary](/handbook/people-operations/global-compensation/) at GitLab is modeled using our [Compensation Principles](/handbook/people-operations/global-compensation/#compensation-principles) to [pay local rates](/handbook/people-operations/global-compensation/#paying-local-rates). 

All annual base pay only roles are seen within the [Compensation Calculator](/handbook/people-operations/global-compensation/calculator/).

## Bonuses 

* [Discretionary Bonus](/handbook/incentives/#discretionary-bonuses)
* [Referral Bonus](/handbook/incentives/#referral-bonuses)
* [Directors](/handbook/people-operations/global-compensation/#director-compensation), Executives, and certain Sales roles also have a variable bonus in addition to their base salary. 

## Stock Options

[Stock Options](/handbook/stock-options/) are available to all team members at GitLab and are not adjusted based on location.

## Perks 

* [Office Equipment and Supplies](/handbook/spending-company-money/)
* [Co-working Office Space](/handbook/spending-company-money/#coworking-or-external-office--space)
* [Work-related conferences](/handbook/spending-company-money/#work-related-online-courses-and-professional-development-certifications)
* [Year-end Holiday Party Budget](/handbook/spending-company-money/#year-end-holiday-party-budget)
* [Visiting Grant](/handbook/incentives/#visiting-grant)
  * Grant to travel for fellow GitLabber's [Significant Life Event](https://about.gitlab.com/handbook/incentives/#significant-life-event-grants)

## Total Rewards Schedule

Throughout the year, the compensation and benefits team will need action from leadership and individual contributors. Below is a summary of the major events that will be coming up and affect everyone at GitLab. For more detailed information on the issues the compensation and benefits team is working on, please see the [total rewards issue tracker](https://gitlab.com/gitlab-com/people-ops/total-rewards/issues).

Summary of Events for FY 2020:

| Month    | Events                                                                                                                        |
|----------|-------------------------------------------------------------------------------------------------------------------------------|
| Feb 2020 | Audit FY 2021 Annual Compensation Review                                                                                      |
|          | [Canada Benefits Implementation](https://gitlab.com/gitlab-com/finance/-/issues/1432)                                         |
| Mar 2020 | [Job Family Alignment](https://gitlab.com/gitlab-com/people-group/Compensation/issues/79)                                     |
|          | Exchange Rate Fluctuations effective 2020-03-01                                                                               |
| Apr 2020 | [Compensation Philosophy Training](https://gitlab.com/gitlab-com/people-group/Compensation/-/issues/107)                      |
| May 2020 | TBD                                                                                                                           |
| Jun 2020 | Benefits Survey Released FY21                                                                                                 |
| Jul 2020 | [Benefits Survey Results FY21 Analyzed and added to Handbook](/handbook/benefits/#global-benefits-survey-results)             |
| Aug 2020 | Catch-up Compensation Review Manager Review (Compaas)                                                                         |
| Sep 2020 | Catch-up Compensation Review Effective 2020-09-01                                                                             |
| Oct 2020 | Compensation Training TBD                                                                                                     |
| Nov 2020 | [Compa Group Reviews](/handbook/people-group/global-compensation/#compa-group)                                                |
| Dec 2020 | Annual Comp Review Inputs Evaluated/Proposed to Compensation Group for FY22                                                   |
|          | Divsion and Department Impact Analysis shared with People Business Partner Team                                               |
| Jan 2021 | Annual Comp Review changes finalized for 2021-02-01 effective date                                                            |
|          | Manager Review (Compaas)                                                                                                      |

