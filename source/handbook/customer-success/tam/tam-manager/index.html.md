---
layout: markdown_page
title: "TAM Manager Handbook"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

View the [TAM Handbook homepage](/handbook/customer-success/tam/) for additional TAM-related handbook pages.

---

# Welcome to the TAM Manager Handbook

## Purpose

This page provides an overview on relevant TAM Managers processes.

## Review Processes

Below are some of the top processes to be performed by TAM Managers on a recurring cadence. The overall goal is to ensure the TAM Manager has a strong grasp on their team's group of customers through various data points (health, Support tickets, Onboarding objects, renewals, etc.). Resources such as Dashboards and videos are included for easy reference.

### Review Checklist

For TAM Managers, here is a short checklist to assist in the review:

* [Account assignment:](/#account-assignment) Accounts have been reviewed and are assigned or are in review
* [Onboarding Objects:](/#onboarding-object-review) Customers in the Onboarding phase have been reviewed and onboarding status and notes are complete
* [Success Plans:](/#success-plan-review) Customers have a Success Plan, it is being communicated with the customer, and the TAM is using it as the guiding document
* [Triage and Health:](/#triage-and-health-review) Health for all TAM-assigned accounts have been updated within the last month, and any accounts in Triage are being evaluated
* [Renewal Review:](/#renewal-review) Upcoming customer renewals have been reviewed — the TAM has updated customer health and communicated with the SAL and SA for the upcoming renewal


### Account Assignment

Accounts are assigned at point of sale by the TAM Manager when a new Account in their region fits the following criteria:

1. ARR: is > 50k ARR
1. Segment: Mid-Market or Enterprise
1. Product purchase is Premium/Silver or higher

When an Account meets the critera, the following will happen:

* The new account will now appear on the TAM Manager's Dashboard
* A notification to initialize a review of the new account within the TAM Manager's territory
* An [Onboarding object](/handbook/customer-success/using-salesforce-within-customer-success/#onboarding-objects) will be created for the assigned TAM (if the TAM field is populated)

While the SAL owns the transition-to-TAM aspect, the TAM Manager will then ensure the new account is assigned to a TAM and Onboarding has commenced.

##### Resources
* [Onboarding Object Handbook](/handbook/customer-success/using-salesforce-within-customer-success/#onboarding-objects)
* [Customer Onboarding Dashboard](https://gitlab.my.salesforce.com/01Z4M000000skrQ)
* [TAM Regional Dashboard](https://gitlab.my.salesforce.com/01Z4M000000slMT)


### Onboarding Object Review

On a recurring basis the TAM Manager should review their team's open Onboarding objects  (if there are any unassigned Onboarding objects, the TAM Manager should assign to the respective TAM). The Onboarding objects should then be reviewed with each individual in one-on-one meetings. The goal of the Onboarding object is to ensure a successful customer outcome during onboarding and is measured by the the time to value metrics described in the handbook.

##### Resources
* [Account Onboarding Handbook](/handbook/customer-success/tam/onboarding/)
* [TAM Regional Dashboard](https://gitlab.my.salesforce.com/01Z4M000000slMT)
* [Customer Onboarding Object Review video](https://www.youtube.com/watch?v=OWXaRB8FjC0&feature=youtu.be)


### Success Plan Review

On a regular basis the TAM Manager should review their team's Success Plans. Success Plans [EBRs](/handbook/customer-success/tam/ebr/), which the TAM Manager should also review with their team. At a minimum, the review should include:

1. What is the customer's documented strategy and is that aligned to why they bought?
1. Are all parties (SAL/AE, SA, TAM, customer) aligned on the stated goals?
1. If the objectives are met, are they in the customer's best interest?
1. Are the outcomes measurable (either as a deliverable or quantifiable)?
1. Is the customer on track to meet or exceed their business outcomes?
1. Are the next steps for demonstrating value defined and in place?

The TAM Manager should then work with their team to help the TAM drive up and demonstrate value to the customer.

##### Resources
* [Success Plan Handbook](https://about.gitlab.com/handbook/customer-success/tam/success-plans/)
* [TAM Regional Dashboard](https://gitlab.my.salesforce.com/01Z4M000000slMT)


### Triage and Health Review
On at least a monthly basis, review accounts within your region for upcoming one-one-ones and the team triage call. This includes reviewing:

1. The GitLab Triage board for accounts in triage
1. The Salesforce Region Dashboard for
   1. Red or Amber accounts to inquire about in upcoming one-on-ones
   1. Outdated customer health reviews to discuss in upcoming one-on-ones

##### Resources
* [Account Triage Handbook](/handbook/customer-success/tam/triage/)
* [Account Triage Board](https://gitlab.com/gitlab-com/customer-success/account-triage/-/boards/703769)
* [TAM Regional Dashboard](https://gitlab.my.salesforce.com/01Z4M000000slMT)


### Renewal Review
At least twice-monthly the TAM Manager should review their region's dashboard for upcoming renewals and review questions such as:

1. Which renewals are coming this quarter?
1. Which renewals are coming in the next 2-3 quarters?
1. What is the health of the accounts?
1. Is the health of each account up to date?
1. Based on this analysis, what are my next steps for the customer's success and our Gross and Net Retention goals?

The TAM Manager should then work with their TAM to ensure collaboration with the GitLab team (SAL/AE, SA, and TAM) for a successful renewal.

##### Resources
* [Renewal Review Handbook](/handbook/customer-success/tam/renewals/)
* [TAM Regional Dashboard](https://gitlab.my.salesforce.com/01Z4M000000slMT)
* [Customer Health Dashboard](https://gitlab.my.salesforce.com/01Z4M000000skuA)
