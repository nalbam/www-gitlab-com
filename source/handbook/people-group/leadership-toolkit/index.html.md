---
layout: handbook-page-toc
title: "Leadership Toolkit"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

The Leadership Toolkit provides tools and resources to assist people managers (1+ direct report) in effectively leading and developing team members. The goal of the toolkit is to enhance clear communication, and increase team member engagement and retention, while leveraging best practices and shared learnings. The toolkit will continually evolve, and we invite you to contribute!

See resources organized by topic below:

| Topic | Resource| Summary| 
| ----- | ---------------------------------------|--------|
| New-to-Management Resources| *[New Manager Enablement Program](/handbook/people-group/learning-and-development/#new-manager-enablement-program) <br> * [New to Leadership and Management](/handbook/people-group/leadership-toolkit/new-to-leadership-and-management)| * Resource for all team members interested in or transitioning to a manager role <br> * Specific guidance for new managers or leaders at GitLab on your responsibilities and best practices |
| Team Member Engagement| * [Leadership Engagement Check-in](https://about.gitlab.com/handbook/people-group/leadership-toolkit/Leadership-Engagement-Check-in/)| * Tool that can help managers better understand a team member's engagement level and improve retention on their teams. |
| Career Development| * [Career Development Conversations](https://about.gitlab.com/handbook/people-group/leadership-toolkit/career-development-conversations/)| * Guidance on when and how to have career conversations with team members|
| Compensation| * [Annual Compensation - Communication Guidelines](/handbook/people-group/leadership-toolkit/compensation-review)| * Guidance for managers on how to deliver compensation decisions made in the compensation review process| 
| Workforce Planning| * [Workforce planning and SWOT analysis](/handbook/people-group/leadership-toolkit/workforce-planning-and-swot-analysis)| * Guidance on how to analyze current team member role alignments, determining future team member needs, identifying the gap between the present and future, and implementing solutions so the division/departments can accomplish its mission, goals, and objectives |
| Conducting 1-1s | * [Conducting 1-1s](https://about.gitlab.com/handbook/leadership/1-1/) | Guidance for managers on how to effectively conduct 1-1 meetings with team members|

