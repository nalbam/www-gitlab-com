---
layout: handbook-page-toc
title: "SDR Sales Alignment"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Purpose / Objective

The purpose of the Global Sales-SDR Alignment Framework is to provide a guideline that helps identify goals, provide better alignment, and highlight expectations for both the Sales Development Representative (SDR) and the Strategic Account Leader (SAL)/Account Executive (AE). 

The alignment framework is designed to facilitate a successful relationship between SDRs and Sales ensuring both are properly positioned to achieve their goals. 
Reference: [Sales Development](/handbook/marketing/revenue-marketing/sdr/) and [Sales](/handbook/sales/) page in Handbook for additional details.

## Goals / Targets 

#### [Criteria for Sales Accepted Opportunity (SAO)](/handbook/business-ops/resources/#criteria-for-sales-accepted-opportunity-sao)
{:.no_toc }

Enterprise (Sales Segment = `Large`) SDRs have a target to produce 8 SAOs per month and Commercial (Sales Segment = `Mid-Market` &/or `SMB`) SDRs have a target to produce 12 SAOs per month. Ramping SDRs have a target of 0 in their first month (onboarding/building foundation), 4 SAOs in month 2 and then 8 SAOs in month 3 (considered fully ramped).   
SAOs will come from both inbound and outbound leads and activities. See [SDR Compensation and Quota](/handbook/marketing/revenue-marketing/sdr/#sdr-compensation-and-quota) for more details.   

SDRs will be required to qualify inbound leads and outbound contacts, leverage Force Management Command of Message principles, create Initial Qualifying Meetings (IQMs), update Salesforce (SFDC) with all notes/next steps and introduce the Sales into opportunities.

When and if an opportunity is accepted and moved to SAO but later needs to be merged to a larger parent opportunity within an account, the sales executive should follow these steps:
Move Opportunity to Stage 8-Closed Lost. Select Closed Lost Reason as Merged into another opportunity and select Save. Once saved, copy and past the parent opportunity (one that it was merged into) into the Closed Lost Details field on the opportunity

## [SDR & Sales Alignment](https://docs.google.com/spreadsheets/d/1IYrjadCIIlJ_wJkwocAjtYdc-2Dz8jXh-W9A8jnamlg/edit#gid=0)

#### Expectations and Strategic Process

#### Communication
{:.no_toc }

Required: Weekly SDR-Sales Sync Meeting - Historically, our SDR/Sales pairings who have incorporated this meeting into their week, have been the most successful at driving new pipeline. 

1. Schedule a 30 min - 1 hour weekly meeting. The purpose of the meeting is to strategize on account plans for outbound prospecting, review lists of key personas, discuss messaging approach, as well as discuss inbound traffic, review inbound lead flow, etc.
1. Proposed Agenda for weekly syncs:
     1. SDR Inbound lead flow review (P1-P8/event-list-upload slack channel) and Outbound ICP’s actioned, and Outreach sequence review. What traction has been made at prioritized accounts? Any roadblocks?
     1. Sales review personal traction at prioritized accounts, review existing Opps, roadblocks, etc.
     1. Discuss any upcoming IQMs and IQMs that should be SAOs but are still in “pending” status
     1. Discuss any SAOs that have gone dark (Sales has made at least 6 attempts to contact: 3 calls/3 emails)
     1. Field & Corporate Marketing Issue Board review/plan
     1. Items to review/follow up on at next sync (SDR and Sales action items)


1. For the first initial team meeting, create a [working agreement](https://docs.google.com/document/) and weekly agenda outline that the SDR and Sales can agree upon. 
     1. Best Practice: SDR and Sales connect 10min pre-IQM to review notes and connect post IQM for a brief retro to discuss notes, follow-up and SAO status.
     1. Highly Encouraged, but not required is for SDR or Sales to create a Slack channel and invite the SDR, Sales Executive, Area Sales Manager, SDR Manager, Solutions Architect, and Technical Account Manager. The purpose of this channel is to have high-level communication regarding leads and opportunities as well as discuss questions. 
     1. Please use chatter in SFDC to communicate changes and requests on existing opportunities. 

#### Process Workflow

1. SDR to nurture and qualify all inbound MQLs in their territory. Discuss lead flow with your Sales Executive.
1. SDR to prospect into target accounts and provide Sales with updates/traction. SDR should share lists of key personas (ICP’s) being inputted into Outreach for prospecting.
1. Sales should provide feedback on outbound Outreach.io sequences that SDR creates.
     1. Please note: SDRs do not create quotes, directly manage accounts and should not get stuck on a small list of accounts for too long.
1. SDR to seek guidance and direction from Sales in regards to both inbound and outbound leads that the SDR is qualifying.
     1. Review and apply CoM principles 
     1. A/B Testing in Outreach sequences
1. SDR to review regional field marketing campaign board with Sales to form a strategy on how to best leverage recent and upcoming events.
     1. [Field Marketing worldwide view](https://gitlab.com/groups/gitlab-com/marketing/-/boards/915336): to find your regional view, please use the menu bar in the upper left corner of the board view
     1. [SDR: Field Marketing Event Outreach & Follow Up](https://gitlab.com/gitlab-com/marketing/sdr/issues/197#trophy-outreach-and-follow-up-sdr)
     1. [Preparing for Field Events](/handbook/marketing/revenue-marketing/sdr/#preparing-for-field-events/)
1. SDR to inform Sales of pending SAOs (in meeting and/or SFDC chatter)
1. Review status of pending IQM’s, qualifying leads, target accounts the Sales has provided the SDR, SDR outbounding activities, and marketing events. 
1. SDRs should always aim to attend the initial meeting (IQM) so they can introduce the prospect to the Sales team (when possible, Chorus notetaker should also be included). If for some reason the SDR can not join, this should be communicated to the Sales team in advance. The SDR will be responsible for:
     1. Greeting and explaining Chorus. "Hi, I'm sure you see GitLab notetaker on this call. We use a tool called Chorus that aids us in note taking and for training purposes. Do you have a problem with our call being recorded? I can always turn the recording off."
     1. The SDR should begin the meeting by recapping what they have discussed so far with the prospect, reviewing proposed agenda items that were included in the calendar invite and introducing the Sales Executive who will dive deeper into qualification/discovery. 
     1. SDR assists with making sure the last 5 minutes of the meeting is reserved for setting up next steps.
     1. SDR responsible for updating opportunity in SFDC with notes from call and chattering Sales Executive on opportunity for either advancement or closure. 

#### Account Prioritization 

1. Sales to work with SDR on target accounts for strategic outbound prospecting purposes.
     1. Discuss “wide” vs “deep” net approach and agree on a good capacity for SDR to begin with. Typically, new SDR’s start with 2-4 accounts per Sales Executive and add additional from there depending on personal bandwidth. Targeted ICP contact lists are pulled for each outreach sequence that the SDR creates.
     1. Sales Executives share a list of accounts in their territory with SDR. They should review and prioritize a “top 10” list and discuss which accounts SDR should start outreach to first. Prioritization categories include but are not limited to: CE Users (version.gitlab.com), Industry Vertical, Executive/Board Connections, Current marketing engagement with account, etc. 
     1. Since most SDR/Sales Team pairings have 2:1 mapping, SDR’s should discuss their plan for equal attention/work load for each Sales Executive, as well as how many accounts they’d like to initially start with.

### Quarterly Business Reviews

It is highly encouraged for Sales to involve their SDR in any QBR prep, as well as invite them to their presentation. SDR’s should carve out time to assist with Sales presentation slides.