---
layout: markdown_page
title: "Virtual Events"
---

## Overview

This page focuses on the different types of Virtual Events run by the Marketing Programs team. 

## Who to work with

Virtual events that fall within the scope of an integrated campaign will be supported by the MPM DRI for that integrated campaign. Virtual events that fall outside the scope of an integrated campaign will be supported by @aoetama.

For a list of active integrated campaigns and MPM DRIs for each campaign, please reference the [Integrated Campaigns handbook page](/handbook/marketing/campaigns/). If you have any questions please post in the [#marketing_programs Slack channel](https://gitlab.slack.com/messages/CCWUCP4MS).

## Types of Virtual Events in GitLab

### GitLab Hosted Virtual Events

* **Webcast:** This is a Marketing Programs hosted virtual event with `Webcast` type configuration in zoom. This is the preferred setup for larger GitLab hosted virtual event (>200 attendees) that **requires registration** due to the integration with Marketo for automated lead flow and event tracking. GitLab hosted webcast type is a single room virtual event that allows multiple hosts. For this virtual event type, MPM supports program creation, driving registration, infrastructure, live moderation, and followup.
*[Step by step intructions for execution](https://about.gitlab.com/handbook/marketing/revenue-marketing/digital-marketing-programs/marketing-programs/virtual-events/webcast/)*
* **Meeting w/wo Breakout:** This is a lighter weight virtual event that can be hosted on GitLabber's personal zoom. This is recommended for smaller virtual events (200 attendees max) and **allows you to break the audience into smaller groups during the event. We can track registration, but there is NO Marketo integration, which requires manual list upload to Marketo post event. For this virtual event type, no MPM program support pre/post event will be required.
*[Step by step instructions for execution](https://about.gitlab.com/handbook/marketing/revenue-marketing/digital-marketing-programs/marketing-programs/virtual-events/meetings-with-without-breakout/index.html#project-planning)*
* **Virtual Workshop:** This virtual event is a combination of the aforementioned Webcast + Meeting w/wo breakout type. This is recommended for GitLab hosted virtual events when you need to track registration for a main event AND session(s). This requires internal session hosts to create their own meetings, activating specific settings, and the creation of a session sheet to allow attendees to navigate to desired session and register before joining. For this virtual event type, MPM supports program creation, driving registration, webcast zoom set up, live moderation, and followup. Requestors sets up the meeting(s) with breakout zoom set up.
*See instructions above based on the type you are responsible for executing*

#### GitLab Hosted Virtual Events Menu

|                           | Webcast | Meeting w/wo Breakout |  Virtual<br>Workshop  |
|---------------------------|---------|---------------------|-----------------------|
| SLA                       | 45 BD   | 5 BD                | 45 BD                 |
| Type                      | Webcast | Meeting             | Webcast + <br>Meeting |
| Tracking                  | Yes     | No                  | Yes                   |
| LP/Registration           | Yes     | Optional<br>(Zoom)  | Yes                   |
| Mktg<br>Promotion         | Yes     | No                  | Yes                   |
| Mktg<br>Moderate          | Yes     | No                  | Yes <br>(webcast)     |
| Mktg <br>Followup         | Yes     | No                  | Yes                   |
| Breakout<br>Rooms         | No      | Yes                 | Yes<br>(Meeting)      |
| Polls/Q&A                 | Yes/Yes | Yes*/No             | Yes/Yes               |
| Confirmation/<br>Reminder | Yes/Yes | Yes/No*             | Yes/Yes               |

**FAQ & Notes:**
* Breakout rooms CANNOT be selected by attendees. Only the host can add attendees to rooms. Within each breakout room, Co-hosts can record when given permission by the host.
* Virtual workshop should only be used when you want attendees to have the option to join one of many sessions happening at once. This style virtual event puts responsibility on the attendee to manually join their desired session. If you select this option, we recommend to NOT switch back and forth between main session (zoom webcast) and session (zoom meeting) multiple times due to a potential poor user experience.
* For Virtual workshops, the session host is responsible for the setup, moderating, recording (GitLab Unfiltered - Unlisted), and attendee report of their own breakout session.

### Sponsored Virtual Events

* **Sponsored demand gen webcast:** This is webcast hosted on an external partner/vendor platform (e.g: DevOps.com). The partner/vendor is responsible for driving registration, moderating and hosting the webcast on their platform. Mktg-OPs will be responsible for uploading the list to our database and MPMs will be responsible for sending post-event follow-up emails.
* **Virtual Conference:** This is not a webcast but rather a virtual conference where we pay a sponsorship fee to get a virtual booth and sometimes a speaking session slot. Marketing programs will primarily be responsible for sending the post-event follow-up emails.

### Virtual events calendar

The calendar below documents all scheduled virtual events.

<figure>
<iframe src="https://calendar.google.com/calendar/b/1/embed?showPrint=0&amp;height=600&amp;wkst=1&amp;bgcolor=%23FFFFFF&amp;src=gitlab.com_1qve6g81dp91r9hevtkfd9r098%40group.calendar.google.com&amp;color=%23B1365F&amp;ctz=America%2FLos_Angeles" style="border-width:0" width="800" height="600" frameborder="0" scrolling="no"></iframe>
<figure/>


## Zoom Capabilities for GitLab Hosted Virtual Events

 
|                                       | Zoom Webcast                | Zoom Meeting                                  |
|---------------------------------------|-----------------------------|-----------------------------------------------|
| Screenshare                           | Yes                         | Yes                                           |
| Videoshare                            | Host/Panelist               | All Participants                              |
| Participant<br>List                   | Visible to<br>host/panelist | All participants                              |
| Multiple Hosts                        | Yes                         | Yes                                           |
| Attendee Limit                        | 500                         | 200                                           |
| Registration<br>Zoom                  | Yes<br>(Optional)           | Yes<br>(Optional)                             |
| Registration<br>Marketo               | Yes                         | No                                            |
| Marketo<br>Integration                | Yes                         | No                                            |
| Automated Tracking<br>and Lead Flow   | Yes                         | No                                            |
| Confirmation Email                    | Yes <br>(from Zoom)         | Yes<br>(from Zoom)                            |
| Auto Reminder<br>Email Send           | Yes<br>(from Zoom)          | No*                                           |
| Practice Session                      | Yes                         | Yes*<br>(requires activating<br>waiting room) |
| Breakout Rooms /<br>Max participants  | 0 / 0                       | 50 / 200                                      |
| Polls                                 | Yes                         | Yes                                           |
| Q&A                                   | Yes                         | No                                            |
| Chat                                  | Yes                         | Yes                                           |
| Livestream                            | Yes                         | Yes                                           |
  
  
**⚠ Note:**
**GitLab hosted virtual events will need to fit into either: Zoom Webcast, Zoom Meeting, or Zoom Webcast + Zoom Meeting (including registration) Type**

## Below is a step by step guide on how to request Marketing Programs support for a virtual event.

**⚠ Note: Virtual Event requests should be submitted no less than 45 business days before the event date so the new request can be added into the responsible Marketing Program Manager's (MPM) workflow and to ensure we have enough leeway for promotion time.**

#### Step 1: Create a [virtual event request issue](https://gitlab.com/gitlab-com/marketing/digital-marketing-programs/blob/master/.gitlab/issue_templates/MPM_VirtualEvent_Request.md) in the digital marketing programs project.
*  Please use the `MPM_VirtualEvent_Request.md` issue template linked above
*  Please put the date of the virtual event as a due date
*  Please specify the type of virtual event
*  @ mention MPM in the issue comment to confirm the requested date is feasible
*  MPM will check the requested date against [the virtual events calendar](https://calendar.google.com/calendar?cid=Z2l0bGFiLmNvbV8xcXZlNmc4MWRwOTFyOWhldnRrZmQ5cjA5OEBncm91cC5jYWxlbmRhci5nb29nbGUuY29t) to make sure there is no overlapping virtual event that has been pre-scheduled
*  If the requested date is feasible and the speakers have been secured the Marketing Programs team will change the status label from `status:plan` to `status:wip`, add the `Webcast` label and applicable `FY..` label (to make sure this appears on the [webcast issue board](https://gitlab.com/groups/gitlab-com/marketing/-/boards/922606?&label_name[]=Webcast))

#### Step 2: MPM will create the Virtual Event EPIC.
*  When "status:wip" is on the issue and necessary elements are documented, MPM creates epic for the virtual event.
*  Naming convention: [Virtual Event Name] - [3-letter Month] [Date], [Year]
*  MPM copy/pastes code below into the epic

```
## [Main Issue >>]()

## [GANTT >>]()

## [Copy for landing page and emails >>]() - [template](https://docs.google.com/document/d/1j43mf7Lsq2AXoNwiygGAr_laiFzmokNCfMHi7KNLjuA/edit)

## Event Details
  * `place details from the event issue here`
  * [ ] [main salesforce campaign]()
  * [ ] [main marketo program]()

## Issue creation

* [ ] Secure presenters and dry runs issue created - MPM
* [ ] Only for Virtual sponsorship and 3rd Party Hosted webcast: List clean and upload issue created - MOps
* [ ] Landing page issue created - MPM
* [ ] Optional: New design assets issue created for the design team - MPM
* [ ] Invitation and reminder issue created - MPM
* [ ] Organic social issue created for social media manager - MPM
* [ ] Paid Ads issue created for DMP - MPM
* [ ] PathFactory request issue created - MPM
* [ ] Follow up email issue created - MPM
* [ ] Add to nurture stream issue created - MPM
* [ ] On-demand switch issue created - MPM

cc @jburton to create list upload issue (Only for Virtual sponsorship and 3rd Party Hosted webcast)

```

#### Step 3: MPM will create the necessary MPM support issues (linked in table below) and add to epic.

The table below shows execution tasks related to various types of Virtual Events along with the DRI for each task.

<div class="tg-wrap">
<table class="tg">
  <tr>
    <th class="tg-k6pi">Tasks</th>
    <th class="tg-k6pi">Due dates</th>
    <th class="tg-yw4l">Hosted demand gen webcast</th>
    <th class="tg-yw4l">Sponsored demand gen webcast</th>
    <th class="tg-yw4l">Sponsored Virtual Conference</th>
    <th class="tg-yw4l">Account specific webcast</th>
  </tr>
  <tr>
    <td><strong> <a href="https://gitlab.com/gitlab-com/marketing/digital-marketing-programs/blob/master/.gitlab/issue_templates/%20MPM-00-secure-presenters_dryruns.md">Secure presenters</a></strong></td>
    <td>-45 business days</td>
    <td>MPM or Requestor</td>
    <td>Requestor</td>
    <td>Requestor</td>
    <td>SALs</td>
  </tr>
  <tr>
    <td><strong> Create MPM Epic, issues, and fill out GANTT</strong></td>
    <td>-43 business days</td>
    <td>MPM</td>
    <td>MPM</td>
    <td>MPM/td>
    <td>MPM</td>
  </tr>
  <tr>
    <td><strong> Host prep call with internal team</strong></td>
    <td>-40 business days</td>
    <td>N/A</td>
    <td>N/A</td>
    <td>N/A</td>
    <td>Only for large segment accounts: MPM</td>
  </tr>
   <tr>
    <td><strong><a href="https://gitlab.com/gitlab-com/marketing/digital-marketing-programs/blob/master/.gitlab/issue_templates/MPM-02-landing-page.md"> Landing page copy due </a></strong></td>
    <td>-35 business days</td>
    <td>Requestor or Presenter</td>
    <td>Requestor</td>
    <td>Requestor</td>
    <td>SALs</td>
  </tr>
  <tr>
    <td><strong><a href="https://gitlab.com/gitlab-com/marketing/corporate-marketing/issues/new?nav_source=navbar">New design assets delivered</a></strong></td>
    <td>-35 business days</td>
    <td>Design</td>
    <td>Design</td>
    <td>Design</td>
    <td>Design</td>
  </tr>
  <tr>
    <td><strong> <a href="https://gitlab.com/gitlab-com/marketing/digital-marketing-programs/blob/master/.gitlab/issue_templates/MPM-02-landing-page.md"> Create landing page MR </a> </strong></td>
    <td>-33 business days</td>
    <td>MPM</td>
    <td>N/A</td>
    <td>N/A</td>
    <td>MPM</td>
  </tr>
  <tr>
    <td><strong> Set up tracking in SFDC, Marketo, and Zoom </strong></td>
    <td>-33 business days</td>
    <td>MPM</td>
    <td>MPM</td>
    <td>MPM</td>
    <td>MPM</td>
  </tr>
   <tr>
    <td><strong> Drive registration - create issue for blog post promotion </strong></td>
    <td>-30 business days</td>
    <td>Optional: Requestor</td>
    <td>Only if we have paid ads budget: Requestor</td>
    <td>Only if we have paid ads budget: Requestor</td>
    <td>N/A</td>
  </tr>
  <tr>
    <td><strong> Paid Ads LIVE </strong></td>
    <td>-30 business days</td>
    <td>DMP; MPM creates the Paid Ads request issue</td>
    <td>Only if we gave paid ads budget: DMP; Requestor creates the Paid Ads request issue</td>
    <td>Only if we gave paid ads budget: DMP; Requestor creates the Paid Ads request issue</td>
    <td>N/A</td>
  </tr>
  <tr>
    <td><strong><a href="https://gitlab.com/gitlab-com/marketing/digital-marketing-programs/blob/master/.gitlab/issue_templates/MPM-03B-virtualevent-promotion.md"> Promote in bi-weekly newsletter </a> </strong></td>
    <td>-21 business days</td>
    <td>MPM</td>
    <td>Requestor</td>
    <td>Requestor</td>
    <td>N/A</td>
  </tr>
  <tr>
    <td><strong><a href="https://gitlab.com/gitlab-com/marketing/corporate-marketing/blob/master/.gitlab/issue_templates/social-event-request.md"> Organic social promotion event support </a> </strong></td>
    <td>-21 business days</td>
    <td>MPM creates organic social request issue</td>
    <td>Requestor creates organic social request issue</td>
    <td>Requestor creates organic social request issue</td>
    <td>Optional(to be discussed in prep call): MPM creates organic social request issue</td>
  </tr>
  <tr>
    <td><strong><a href="https://gitlab.com/gitlab-com/marketing/digital-marketing-programs/blob/master/.gitlab/issue_templates/MPM-03-invitations-reminder.md"> Marketing email invite copy written </a> </strong></td>
    <td>-21 business days</td>
    <td>MPM</td>
    <td>N/A</td>
    <td>N/A</td>
    <td>N/A</td>
  </tr>
  <tr>
    <td><strong><a href="https://gitlab.com/gitlab-com/marketing/digital-marketing-programs/blob/master/.gitlab/issue_templates/MPM-03-invitations-reminder.md"> Marketing email invite scheduled </a> </strong></td>
    <td>-16 business days</td>
    <td>MPM</td>
    <td>N/A</td>
    <td>N/A</td>
    <td>N/A</td>
  </tr>
    <tr>
    <td><strong> Presentation slides due </strong></td>
    <td>-7 business days</td>
    <td>Requestor or Presenter</td>
    <td>Requestor</td>
    <td>Requestor</td>
    <td>Optional (to be discusseed in prep call): SALs</td>
  </tr>
    <tr>
    <td><strong><a href="https://gitlab.com/gitlab-com/marketing/digital-marketing-programs/blob/master/.gitlab/issue_templates/MPM-04-follow-up-email.md"> Post event follow up email copy written </a></strong></td>
    <td>-5 business days</td>
    <td>MPM</td>
    <td>Requestor</td>
    <td>Requestor</td>
    <td>SALs</td>
  </tr>
    <tr>
    <td><strong><a href="https://gitlab.com/gitlab-com/marketing/digital-marketing-programs/blob/master/.gitlab/issue_templates/%20MPM-00-secure-presenters_dryruns.md">  Host dry run </a> </strong></td>
    <td>-5 business days</td>
    <td>MPM</td>
    <td>Requestor</td>
    <td>Requestor</td>
    <td>MPM</td>
  </tr>
    <tr>
    <td><strong> Host virtual event</strong></td>
    <td>0 business days</td>
    <td>MPM</td>
    <td>3rd party vendor</td>
    <td>3rd party vendor</td>
    <td>MPM</td>
  </tr>
    <tr>
    <td><strong> Moderate webcast</strong></td>
    <td>0 business days</td>
    <td>MPM or Presenter</td>
    <td>3rd party vendor</td>
    <td>N/A</td>
    <td>SALs</td>
  </tr>
    <tr>
    <td><strong><a href="https://gitlab.com/gitlab-com/marketing/digital-marketing-programs/blob/master/.gitlab/issue_templates/MPM-06-on-demand-switch.md"> Upload live webcast recording to youtube</a></strong></td>
    <td>+1 business days</td>
    <td>MPM</td>
    <td>Optional (if available): MPM</td>
    <td>Optional (if available): MPM</td>
    <td>Optional (to be discussed in prep call): MPM</td>
  </tr>
    <tr>
    <td><strong><a href="https://gitlab.com/gitlab-com/marketing/digital-marketing-programs/blob/master/.gitlab/issue_templates/pathfactory_request.md"> Upload youtube recording to PathFactory</a></strong></td>
    <td>+1 business days</td>
    <td>DMP</td>
    <td>Optional (if available): DMP</td>
    <td>Optional (if available): DMP</td>
    <td>Optional (to be discussed in prep call): MPM</td>
  </tr>
    <tr>
    <td><strong><a href="https://gitlab.com/gitlab-com/marketing/digital-marketing-programs/blob/master/.gitlab/issue_templates/MPM-06-on-demand-switch.md"> Convert registration page to on-demand </a> </strong></td>
    <td>+2 business days</td>
    <td>MPM</td>
    <td>Optional (if available): MPM</td>
    <td>Optional (if available): MPM</td>
    <td>Optional (to be discussed in prep call): MPM</td>
  </tr>
    <tr>
    <td><strong><a href="https://gitlab.com/gitlab-com/marketing/marketing-operations/blob/master/.gitlab/issue_templates/event-clean-upload-list.md"> Forward cleaned lead list to MOPs for upload </a></strong></td>
    <td>+2 business days</td>
    <td>N/A</td>
    <td>Requestor</td>
    <td>Requestor</td>
    <td>N/A</td>
  </tr>
    <tr>
    <td><strong><a href="https://gitlab.com/gitlab-com/marketing/marketing-operations/blob/master/.gitlab/issue_templates/event-clean-upload-list.md"> MOPs finished uploading list to SFDC (CRM) </a></strong></td>
    <td>+4 business days</td>
    <td>N/A</td>
    <td>MOPs</td>
    <td>MOPs</td>
    <td>N/A</td>
  </tr>
    <tr>
    <td><strong><a href="https://gitlab.com/gitlab-com/marketing/digital-marketing-programs/blob/master/.gitlab/issue_templates/MPM-04-follow-up-email.md"> Send marketo follow up emails </a></strong></td>
    <td>+2 to +5 business days</td>
    <td>MPM</td>
    <td>MPM</td>
    <td>MPM</td>
    <td>MPM</td>
  </tr>
    <tr>
    <td><strong> Facilitate retrospective </strong></td>
    <td>+7 business days</td>
    <td>MPM</td>
    <td>MPM</td>
    <td>MPM</td>
    <td>N/A</td>
  </tr>
  </table>
</div>


