---
layout: markdown_page
title: "GitHost Migration Working Group"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Attributes

| Property        | Value           |
|-----------------|-----------------|
| Date Created    | January 30, 2020 |
| Target End Date | April 24, 2020|
| Slack           | [#wg_githost_migration](https://gitlab.slack.com/archives/CRKL886F2) (only accessible from within the company) |
| Google Doc      | [GitHost Migration Working Group Agenda](https://docs.google.com/document/d/1O8cF5ylQHJDAXVB3KUoW5FhPRH-RIOpHAOuuuPz0VL0/edit) (only accessible from within the company) |

## Business Goal

Successfully migrate a key customer from GitHost.io to GitLab.com. More details on the specific customer in Google Doc above.

## Exit Criteria (~80%)

1. Define acceptance criteria for customer. => **Done** The customer has stated they want all data migrated across. We have confirmed scope of data included in migration, which is viewable by following the *Migration Features* link [here](https://docs.google.com/document/d/1O8cF5ylQHJDAXVB3KUoW5FhPRH-RIOpHAOuuuPz0VL0/edit#heading=h.1jbhsgk81yat).
1. Identify and deliver issues blocking migration efforts in [this epic](https://gitlab.com/groups/gitlab-org/-/epics/2584). => **~80%** Awaiting sign-off from Quality and Security as detailed in the epic.
1. Complete an end-to-end testing plan and conduct a migration dry-run. => **~80%** Currently completing the dry-run against a "noisy" test instance.
1. Document known risks as part of the migration process. => **90%** Risks listed [here](https://gitlab.com/gitlab-org/manage/-/issues/16366). Working Group to add approval for each risk.
1. Execute on migration for customer. => Expected Start Date 13th April.

## Roles and Responsibilities

| Working Group Role    | Person                | Title                                      |
|-----------------------|-----------------------|--------------------------------------------|
| Executive Stakeholder | Christopher Lefelhocz | Senior Director of Development             |
| Facilitator           | Allison Walker        | Professional Services Project Manager      |
| Functional Lead       | Liam McAndrew         | Backend Engineering Manager, Manage:Import |
| Functional Lead       | Jeremy Watson         | Group Product Manager, Manage              |
| Functional Lead       | Michael Lutz          | Senior Director of Professional Services   |
| Functional Lead       | Paul Harrison         | Security Manager, Security Operations      |
| Member                | Haris Delalić         | Product Manager, Manage:Import             |
| Member                | George Koltsov        | Backend Engineer, Manage:Import            |
| Member                | Désirée Chevalier     | Software Engineer in Test, Manage          |
| Member                | Sanad Liaquat         | Senior Software Engineer in Test, Manage   |
| Member                | Glen Miller           | Professional Services Engineer             |
| Member                | Petar Prokic          | Professional Services Engineer, EMEA       |
| Member                | Sean Hoyle            | Technical Account Manager                  |
| Member                | Luca Williams         | Product Manager, Manage:Spaces             |
| Member                | Lyle Kozloff          | Support Engineering Manager                |
